---
# Don't forget to keep this file updated
# molecule/<scenario>/verify.yml
- name: "Verify:role-install-mariadb"
  hosts: "local:&role-install-mariadb"
  gather_facts: true
  tasks:

    - name: "Get MariaDB service current state"
      register: "install_mariadb__service_status"
      failed_when: "not install_mariadb__service_status.status.ActiveState == 'active'"
      ansible.builtin.systemd:
        name: "mariadb"

    - name: "Check MariaDB connectivity"
      ansible.builtin.wait_for:
        host: "{{ inventory_hostname }}"
        port: "{{ inv__install_mariadb.port }}"
        timeout: 120

    - name: "Check Cluster health"
      when: "inv__install_mariadb__galera_cluster.enabled | default(false)"
      block:
        - name: "Check Galera replciation connectivities"
          loop: "{{ inv__install_mariadb__galera_cluster.node_list }}"
          loop_control:
            loop_var: "node"
          ansible.builtin.wait_for:
            host: "{{ node }}"
            port: "4567"
            timeout: 120

        - name: "Set expected cluster size"
          ansible.legacy.set_fact:
            expected_cluster_size: "{{ inv__install_mariadb__galera_cluster.node_list | length }}"

        - name: "Get Galera Cluster Size from MySQL"
          register: "galera_cluster_size_result"
          failed_when: "galera_cluster_size_result.stdout != expected_cluster_size"
          changed_when: "galera_cluster_size_result.rc != 0"
          ansible.builtin.shell: "mysql -u root --password='{{ inv__install_mariadb.new_password }}' --execute=\"SHOW STATUS LIKE 'wsrep_cluster_size';\" | grep -oP '\\d+'"

        - name: "Get Galera Cluster State from MySQL"
          register: "galera_cluster_synced_result"
          failed_when: "galera_cluster_synced_result.stdout_lines[1] != \"wsrep_local_state_comment\tSynced\""
          changed_when: "galera_cluster_synced_result.rc != 0"
          ansible.builtin.command: "mysql -u root --password='{{ inv__install_mariadb.new_password }}' --execute=\"SHOW STATUS LIKE 'wsrep_local_state_comment';\""

- name: "Verify:role-install-maxscale"
  hosts: "local:&role-install-maxscale"
  gather_facts: true
  tasks:

    - name: "Get MaxScale service current state"
      register: "install_maxscale__service_status"
      failed_when: "not install_maxscale__service_status.status.ActiveState == 'active'"
      ansible.builtin.systemd:
        name: "maxscale"

    - name: "Check MaxScale connectivities"
      block:
        - name: "Check MaxScale connectivity: admin GUI"
          when: "inv__install_maxscale__admin.enabled | default(false)"
          ansible.builtin.wait_for:
            host: "{{ inventory_hostname }}"
            port: "{{ inv__install_maxscale__admin.port }}"
            timeout: 120

        - name: "Check MaxScale connectivity: database listener"
          ansible.builtin.wait_for:
            host: "{{ inventory_hostname }}"
            port: "{{ inv__install_maxscale__listener.port }}"
            timeout: 120

    - name: "Show databases, with the MaxScale proxy and user"
      register: "output"
      changed_when: "output.rc != 0"
      vars:
        ssl_options: "--ssl --ssl-ca='{{ inv__install_maxscale.ssl_path }}/{{ inv__install_maxscale__mariadb.ssl_ca }}'"
        mtls_options: "{{ '' if not inv__install_maxscale__listener.ssl_client_auth | default(false) else '--ssl-cert=' ~ inv__install_maxscale.ssl_path ~ '/' ~ inv__install_maxscale__listener.ssl_crt ~ ' --ssl-key=' ~ inv__install_maxscale.ssl_path ~ '/' ~ inv__install_maxscale__listener.ssl_key }}"
        host_options: "-h localhost -P {{ inv__install_maxscale__listener.port }}"
      ansible.builtin.shell: |
        mysql {{ host_options }} {{ ssl_options | default('') }} {{ mtls_options | default('') }} -u {{ inv__install_maxscale__mariadb.maxscale_user }} --password='{{ inv__install_maxscale__mariadb.maxscale_password }}' --execute="SHOW DATABASES;"

    - name: "Show databases, with the MaxScale proxy and user"
      register: "output"
      changed_when: "output.rc != 0"
      vars:
        ssl_options: "--ssl --ssl-ca='{{ inv__install_maxscale.ssl_path }}/{{ inv__install_maxscale__mariadb.ssl_ca }}'"
        mtls_options: "{{ '' if not inv__install_maxscale__listener.ssl_client_auth | default(false) else '--ssl-cert=' ~ inv__install_maxscale.ssl_path ~ '/' ~ inv__install_maxscale__listener.ssl_crt ~ ' --ssl-key=' ~ inv__install_maxscale.ssl_path ~ '/' ~ inv__install_maxscale__listener.ssl_key }}"
        host_options: "-h localhost -P {{ inv__install_maxscale__listener.port }}"
      ansible.builtin.shell: |
        mysql {{ host_options }} {{ ssl_options | default('') }} {{ mtls_options | default('') }} -u {{ inv__install_maxscale__mariadb.maxscale_user }} --password='{{ inv__install_maxscale__mariadb.maxscale_password }}' --execute="SHOW DATABASES;"

    - name: "Check MariaDB Galera Cluster health, with the MaxScale proxy and user"
      when: "inv__install_maxscale__mariadb.type == 'galeramon'"
      block:
        - name: "Set expected cluster size, SSL options and mTLS options"
          ansible.legacy.set_fact:
            expected_cluster_size: "{{ inv__install_mariadb__galera_cluster.node_list | length }}"
            ssl_options: "--ssl --ssl-ca='{{ inv__install_maxscale.ssl_path }}/{{ inv__install_maxscale__mariadb.ssl_ca }}'"
            mtls_options: "{{ '' if not inv__install_maxscale__listener.ssl_client_auth | default(false) else '--ssl-cert=' ~ inv__install_maxscale.ssl_path ~ '/' ~ inv__install_maxscale__listener.ssl_crt ~ ' --ssl-key=' ~ inv__install_maxscale.ssl_path ~ '/' ~ inv__install_maxscale__listener.ssl_key }}"
            host_options: "-h localhost -P {{ inv__install_maxscale__listener.port }}"

        - name: "Get Galera Cluster Size, from MaxScale mTLS service"
          register: "galera_cluster_size_result"
          failed_when: "galera_cluster_size_result.stdout != expected_cluster_size"
          changed_when: "galera_cluster_size_result.rc != 0"
          ansible.builtin.shell: "mysql {{ host_options }} {{ ssl_options | default('') }} {{ mtls_options | default('') }} -u {{ inv__install_maxscale__mariadb.maxscale_user }} --password='{{ inv__install_maxscale__mariadb.maxscale_password }}' --execute=\"SHOW STATUS LIKE 'wsrep_cluster_size';\" | grep -oP '\\d+'"

        - name: "Get Galera Cluster State, from MaxScale mTLS service"
          register: "galera_cluster_synced_result"
          failed_when: "galera_cluster_synced_result.stdout_lines[1] != \"wsrep_local_state_comment\tSynced\""
          changed_when: "galera_cluster_synced_result.rc != 0"
          ansible.builtin.command: "mysql {{ host_options }} {{ ssl_options | default('') }} {{ mtls_options | default('') }} -u {{ inv__install_maxscale__mariadb.maxscale_user }} --password='{{ inv__install_maxscale__mariadb.maxscale_password }}' --execute=\"SHOW STATUS LIKE 'wsrep_local_state_comment';\""

    - name: "Check MaxScale Admin GUI"
      when: "inv__install_maxscale__admin.enabled | default(false)"
      register: "result"
      failed_when: "result.status != 200"
      ansible.builtin.uri:
        url: "https://{{ inventory_hostname }}:{{ inv__install_maxscale__admin.port }}/"
        method: "GET"
